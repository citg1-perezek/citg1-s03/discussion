package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	/*
	 * Initialization - First stage of the servlet life cycle
	 * The "init" method is used for functions such as connecting to database and initializing values to be used in the context of the servlet.
	 * - loads the servlet.
	 * - creates an instance of the servlet class.
	 * - initializes the servlt instance by calling the init method.
	 * */
	public void init() throws ServletException{
		System.out.println("********************");
		System.out.println("Initiliazed connection to database");
		System.out.println("********************");
	}
	
	//service() handles any type of request, not recommended, and need to specify the actual method to receive the response
	//Changing the method to doPost will only be accessible via a post request (post method in the HTML form)
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("Hello from the calculator servlet.");
	
		/*The parameter names are defined in the form input field*/
		
		/*The parameters are found in the URL as query string (e.g. ?num1=&num2)*/
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		
		int total = num1 + num2;
		
		//The getWriter() is used to print out information in the browser as a response
		PrintWriter out = res.getWriter();
		
		
		//usually prints a string in the browser.
		//getWriter can output not only string but also HTML elements
		out.println("<h1>The total of the two numbers are " + total + "</h1>");
	}
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		PrintWriter out =res.getWriter();
		
		out.println("<h1> You have accessed the get method of the calculator servlet.</h1>");
	}
	
	/*
	 * Finalization - last part of the servlet life cycle that invokes the "destroy" method.
	 * - Clean up of the resources once the servlet is destroyed/unused.
	 * - Closing the connections.
	 */
	public void destroy() {
		System.out.println("********************");
		System.out.println("Disconnected from database");
		System.out.println("********************");
	}
}
	
	
	
	
